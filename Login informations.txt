Le rapport contients les informations nécessaires pour activer l'authentification.

Pour se connecter, utilisez s'il vous plait les informations suivantes :

Username : admin
Password : 123456

N.B: Seulement les utilisateurs qui ont le type "Administrator" ont le droit de se connecter a l'application de gestion du stock