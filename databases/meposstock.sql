-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 23, 2018 at 11:05 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meposstock`
--

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
CREATE TABLE IF NOT EXISTS `inventories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Description` char(150) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`ID`, `product_id`, `Quantity`, `Description`, `Date`) VALUES
(3, 1, 220, 'Description 12', '2018-05-27'),
(43, 1, 7000, 'cfdgfh', '2018-06-13'),
(50, 3, 2000, 'fgfdh', '2018-06-14'),
(51, 3, 15000, 'fgfhgfhjg', '2018-06-18'),
(54, 1, 150, 'grfgh', '2018-06-20'),
(55, 1, 150, 'gte5ty', '2018-06-23');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
CREATE TABLE IF NOT EXISTS `purchases` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `ReceptionDate` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`ID`, `product_id`, `Quantity`, `ReceptionDate`) VALUES
(1, 1, 15, '2018-05-09'),
(2, 1, 200, '2018-06-13'),
(9, 1, 300, '2018-06-14'),
(10, 1, 400, '2018-06-14'),
(11, 1, 250, '2018-06-14'),
(16, 1, 1000, '2018-06-14'),
(17, 3, 700, '2018-06-15'),
(18, 3, 800, '2018-06-15'),
(19, 3, 800, '2018-06-18'),
(20, 3, 800, '2018-06-18'),
(21, 3, 700, '2018-06-18');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inventories`
--
ALTER TABLE `inventories`
  ADD CONSTRAINT `inventories_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `g_vente`.`products` (`ID`);

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `g_vente`.`products` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
