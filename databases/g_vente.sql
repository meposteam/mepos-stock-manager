-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 23, 2018 at 11:05 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `g_vente`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(30) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `Name`, `category_id`) VALUES
(1, 'Electronics', NULL),
(2, 'Hard Drives', 1),
(3, 'External Hard Drives', 2),
(4, 'Internal Hard Drives', 2),
(5, 'SATA Hard Drives', 2);

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

DROP TABLE IF EXISTS `memberships`;
CREATE TABLE IF NOT EXISTS `memberships` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(15) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `memberships`
--

INSERT INTO `memberships` (`ID`, `Name`) VALUES
(1, 'Administrator'),
(4, 'Customer'),
(3, 'Delivery Man'),
(2, 'Seller');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Customer` varchar(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ID`, `Customer`, `product_id`, `Quantity`, `Date`) VALUES
(1, 'Mustapha ELKOJJI', 1, 50, '2018-05-25'),
(2, 'Mustapha ELKOJJI', 1, 100, '2018-06-13'),
(3, 'Mustapha ELKOJJI', 1, 150, '2018-06-13'),
(4, 'Mustapha ELKOJJI', 1, 300, '2018-06-13'),
(6, 'Mustapha ELKOJJI', 3, 200, '2018-06-15'),
(7, 'Mustapha ELKOJJI', 3, 200, '2018-06-15'),
(8, 'Mustapha ELKOJJI', 1, 160, '2018-06-14'),
(9, 'Mustapha ELKOJJI', 1, 160, '2018-06-15'),
(10, 'Mustapha ELKOJJI', 3, 500, '2018-06-18'),
(11, 'Mustapha ELKOJJI', 3, 300, '2018-06-18');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BarCode` char(20) NOT NULL,
  `Name` char(60) NOT NULL,
  `Description` char(150) DEFAULT NULL,
  `Price` decimal(10,0) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `BarCode` (`BarCode`),
  UNIQUE KEY `Name` (`Name`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `BarCode`, `Name`, `Description`, `Price`, `category_id`) VALUES
(1, '4051528143868', 'TOSHIBA USB 3.0', 'External Hard Drive', '649', 4),
(3, '1234567891', 'TOSHIBA', 'frtyr ytr ytr y', '999', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` char(15) NOT NULL,
  `Password` char(60) NOT NULL,
  `Firstname` char(20) NOT NULL,
  `Lastname` char(20) NOT NULL,
  `membership_id` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Username` (`Username`),
  KEY `membership_id` (`membership_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Username`, `Password`, `Firstname`, `Lastname`, `membership_id`) VALUES
(1, 'admin', '123456', 'Mustapha', 'ELKOJJI', 1),
(3, 'mo.salah', '123456789', 'Mohamed', 'SALAH', 2),
(4, 'ahelmy', 'a12345', 'Ahmed', 'Helmy', 4),
(6, 'melkojji', '123789', 'Mustapha', 'ELKOJJI', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`ID`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`ID`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`ID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`membership_id`) REFERENCES `memberships` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
