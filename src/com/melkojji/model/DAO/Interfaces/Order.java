/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.sql.Date;

/**
 * @author Mustapha EL KOJJI
 *
 */
public interface Order {
	public Integer findQuantityByProductAndDateLimit(com.melkojji.model.ManagedBeans.Product product, Date dateLimit) throws Exception;
}
