/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.util.ArrayList;

/**
 * @author Mustapha EL KOJJI
 *
 */
public interface Category {
	public void insert(com.melkojji.model.ManagedBeans.Category category) throws Exception;
	public void update(com.melkojji.model.ManagedBeans.Category category) throws Exception;
	public void delete(com.melkojji.model.ManagedBeans.Category category) throws Exception;
	public ArrayList<com.melkojji.model.ManagedBeans.Category> findAll() throws Exception;
	public com.melkojji.model.ManagedBeans.Category findById(Integer id) throws Exception;
	public com.melkojji.model.ManagedBeans.Category findByName(String name) throws Exception;
}
