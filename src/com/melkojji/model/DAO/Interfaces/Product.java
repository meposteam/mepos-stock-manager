/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.util.ArrayList;

/**
 * @author melko
 *
 */
public interface Product {
	public ArrayList<com.melkojji.model.ManagedBeans.Product> findAll() throws Exception;
}
