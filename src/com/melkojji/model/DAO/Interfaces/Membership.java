/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.util.ArrayList;

/**
 * @author Mustapha EL KOJJI
 *
 */
public interface Membership {
	public ArrayList<com.melkojji.model.ManagedBeans.Membership> findAll() throws Exception;
}
