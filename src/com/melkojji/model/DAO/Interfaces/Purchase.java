/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.sql.Date;
import java.util.ArrayList;

/**
 * @author Mustapha EL KOJJI
 *
 */
public interface Purchase {
	public void insert(com.melkojji.model.ManagedBeans.Purchase purchase) throws Exception;
	public void update(com.melkojji.model.ManagedBeans.Purchase purchase) throws Exception;
	public void delete(com.melkojji.model.ManagedBeans.Purchase purchase) throws Exception;
	public ArrayList<com.melkojji.model.ManagedBeans.Purchase> findAll() throws Exception;
	public Integer findQuantityByProductAndReceptionDateLimit(com.melkojji.model.ManagedBeans.Product product, Date receptionDateLimit) throws Exception;
}
