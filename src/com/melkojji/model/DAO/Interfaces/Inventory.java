/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.sql.Date;
import java.util.ArrayList;

/**
 * @author Mustapha EL KOJJI
 *
 */
public interface Inventory {
	public void insert(com.melkojji.model.ManagedBeans.Inventory inventory) throws Exception;
	public void update(com.melkojji.model.ManagedBeans.Inventory inventory) throws Exception;
	public void delete(com.melkojji.model.ManagedBeans.Inventory inventory) throws Exception;
	public ArrayList<com.melkojji.model.ManagedBeans.Inventory> findAll() throws Exception;
	public com.melkojji.model.ManagedBeans.Inventory findLatestByProductAndDate(com.melkojji.model.ManagedBeans.Product product, Date date) throws Exception;
}
