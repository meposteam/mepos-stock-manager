/**
 * 
 */
package com.melkojji.model.DAO.Interfaces;

import java.util.ArrayList;

/**
 * @author Mustapha EL KOJJI
 *
 */
public interface User {
	public void insert(com.melkojji.model.ManagedBeans.User user) throws Exception;
	public void update(com.melkojji.model.ManagedBeans.User user) throws Exception;
	public void delete(com.melkojji.model.ManagedBeans.User user) throws Exception;
	public ArrayList<com.melkojji.model.ManagedBeans.User> findAll() throws Exception;
	public com.melkojji.model.ManagedBeans.User findByUsername(String username) throws Exception;
}
