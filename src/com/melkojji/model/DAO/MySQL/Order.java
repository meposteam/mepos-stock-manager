/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;
import com.melkojji.model.ManagedBeans.Product;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class Order implements com.melkojji.model.DAO.Interfaces.Order {
	private DAOFactory DAOFactory;
	
	/**
	 * @param DAOFactory
	 */
	public Order(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}
	
	

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Order#findQuantityByProductAndDateLimit(com.melkojji.model.ManagedBeans.Product, java.sql.Date)
	 */
	@Override
	public Integer findQuantityByProductAndDateLimit(Product product, Date dateLimit) throws Exception {
		Integer quantity = 0;
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT SUM(`orders`.`Quantity`) AS `Quantity` FROM `g_vente`.`orders` WHERE `orders`.`product_id` = ? AND `orders`.`Date` >= ? AND `orders`.`Date` <= ?");
		preparedStatement.setInt(1, product.getId());
		preparedStatement.setDate(2, product.getLatestInventory().getDate());
		preparedStatement.setDate(3, dateLimit);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			quantity = resultSet.getInt("Quantity");
		}
		return quantity;
	}

}
