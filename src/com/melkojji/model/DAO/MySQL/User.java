/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class User implements com.melkojji.model.DAO.Interfaces.User {
	private DAOFactory DAOFactory;
	
	
	/**
	 * @param DAOFactory
	 */
	public User(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}
	
	
	

	
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.User#insert(com.melkojji.model.ManagedBeans.User)
	 */
	@Override
	public void insert(com.melkojji.model.ManagedBeans.User user) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `users` (`ID`, `Username`, `Password`, `Firstname`, `Lastname`, `membership_id`) VALUES (NULL, ?, ?, ?, ?, ?);");
		preparedStatement.setString(1, user.getUsername());
		preparedStatement.setString(2, user.getPassword());
		preparedStatement.setString(3, user.getFirstName());
		preparedStatement.setString(4, user.getLastName());
		preparedStatement.setInt(5, user.getMembership().getId());
		preparedStatement.executeUpdate();
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.User#update(com.melkojji.model.ManagedBeans.User)
	 */
	@Override
	public void update(com.melkojji.model.ManagedBeans.User user) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE `users` SET `Username` = ?, `Password` = ?, `Firstname` = ?, `Lastname` = ?, `membership_id` = ? WHERE `users`.`ID` = ?;");
		preparedStatement.setString(1, user.getUsername());
		preparedStatement.setString(2, user.getPassword());
		preparedStatement.setString(3, user.getFirstName());
		preparedStatement.setString(4, user.getLastName());
		preparedStatement.setInt(5, user.getMembership().getId());
		preparedStatement.setInt(6, user.getId());
		preparedStatement.executeUpdate();
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.User#delete(com.melkojji.model.ManagedBeans.User)
	 */
	@Override
	public void delete(com.melkojji.model.ManagedBeans.User user) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `users` WHERE `users`.`ID` = ?;");
		preparedStatement.setInt(1, user.getId());
		preparedStatement.executeUpdate();
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.User#findAll()
	 */
	@Override
	public ArrayList<com.melkojji.model.ManagedBeans.User> findAll() throws Exception {
		ArrayList<com.melkojji.model.ManagedBeans.User> users = new ArrayList<com.melkojji.model.ManagedBeans.User>();
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `users`.`ID`, `Username`, `Password`, `Firstname`, `Lastname`, `membership_id`, `memberships`.`Name` AS `membership_name` FROM `users` JOIN `memberships` ON `users`.`membership_id` = `memberships`.`ID` WHERE 1");
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			com.melkojji.model.ManagedBeans.Membership membership = new com.melkojji.model.ManagedBeans.Membership(resultSet.getInt("membership_id"), resultSet.getString("membership_name"));
			users.add(new com.melkojji.model.ManagedBeans.User(resultSet.getInt("ID"), resultSet.getString("Username"), resultSet.getString("Password"), resultSet.getString("Firstname"), resultSet.getString("Lastname"), membership));
		}
		return users;
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.User#findByUsername(java.lang.String)
	 */
	@Override
	public com.melkojji.model.ManagedBeans.User findByUsername(String username) throws Exception {
		com.melkojji.model.ManagedBeans.User user = null;
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `users`.`ID`, `Username`, `Password`, `Firstname`, `Lastname`, `membership_id`, `memberships`.`Name` AS `membership_name` FROM `users` JOIN `memberships` ON `users`.`membership_id` = `memberships`.`ID` WHERE `username` = ?");
		preparedStatement.setString(1, username);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			com.melkojji.model.ManagedBeans.Membership membership = new com.melkojji.model.ManagedBeans.Membership(resultSet.getInt("membership_id"), resultSet.getString("membership_name"));
			user = new com.melkojji.model.ManagedBeans.User(resultSet.getInt("ID"), resultSet.getString("Username"), resultSet.getString("Password"), resultSet.getString("Firstname"), resultSet.getString("Lastname"), membership);
		}
		return user;
	}
}
