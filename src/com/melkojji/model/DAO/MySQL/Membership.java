/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class Membership implements com.melkojji.model.DAO.Interfaces.Membership {
	private DAOFactory DAOFactory;
	
	
	/**
	 * @param DAOFactory
	 */
	public Membership(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}
	
	

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Membership#findAll()
	 */
	@Override
	public ArrayList<com.melkojji.model.ManagedBeans.Membership> findAll() throws Exception {
		ArrayList<com.melkojji.model.ManagedBeans.Membership> memberships = new ArrayList<com.melkojji.model.ManagedBeans.Membership>();
				Connection connection = this.DAOFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement("SELECT `memberships`.`ID`, `memberships`.`Name` FROM `memberships` WHERE 1");
				ResultSet resultSet = preparedStatement.executeQuery();
				while(resultSet.next()) {
					memberships.add(new com.melkojji.model.ManagedBeans.Membership(resultSet.getInt("ID"), resultSet.getString("Name")));
				}
		return memberships;
	}

}
