/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class Category implements com.melkojji.model.DAO.Interfaces.Category {
	private DAOFactory DAOFactory;
	

	/**
	 * @param DAOFactory
	 */
	public Category(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}

	
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Category#insert()
	 */
	@Override
	public void insert(com.melkojji.model.ManagedBeans.Category category) throws Exception {
		// TODO Auto-generated method stub
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `categories` (`ID`, `Name`, `category_id`) VALUES (NULL, ?, ?);");
		preparedStatement.setString(1, category.getName());
		preparedStatement.setInt(2, category.getParentCategory().getId());
		preparedStatement.executeUpdate();
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Category#update(com.melkojji.model.ManagedBeans.Category)
	 */
	@Override
	public void update(com.melkojji.model.ManagedBeans.Category category) throws Exception {
		// TODO Auto-generated method stub
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE `categories` SET `Name` = ?, `category_id` = ? WHERE `categories`.`ID` = ?;");
		preparedStatement.setString(1, category.getName());
		preparedStatement.setInt(2, category.getParentCategory().getId());
		preparedStatement.setInt(3, category.getId());
		preparedStatement.executeUpdate();
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Category#delete(com.melkojji.model.ManagedBeans.Category)
	 */
	@Override
	public void delete(com.melkojji.model.ManagedBeans.Category category) throws Exception {
		// TODO Auto-generated method stub
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `categories` WHERE `categories`.`ID` = ?;");
		preparedStatement.setInt(1, category.getId());
		preparedStatement.executeUpdate();
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Category#findAll()
	 */
	@Override
	public ArrayList<com.melkojji.model.ManagedBeans.Category> findAll() throws Exception {
		ArrayList<com.melkojji.model.ManagedBeans.Category> categories = new ArrayList<com.melkojji.model.ManagedBeans.Category>();
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `categories`.`ID`, `categories`.`Name`, `categories`.`category_id`, `categories_2`.`Name` AS `category_name` FROM `g_vente`.`categories` LEFT JOIN `g_vente`.`categories` AS `categories_2` ON `categories`.`category_id` = `categories_2`.`ID` WHERE 1");
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			categories.add(new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("ID"), resultSet.getString("Name"), new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("category_id"), resultSet.getString("category_name"), null)));
		}
		return categories;
	}
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Category#findById(java.lang.Integer)
	 */
	@Override
	public com.melkojji.model.ManagedBeans.Category findById(Integer id) throws Exception {
		// TODO Auto-generated method stub
		com.melkojji.model.ManagedBeans.Category category = null;
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `categories`.`ID`, `categories`.`Name`, `categories`.`category_id`, `categories_2`.`Name` AS `category_name` FROM `categories` LEFT JOIN `categories` AS `categories_2` ON `categories`.`category_id` = `categories_2`.`ID` WHERE `categories`.`ID` = ?");
		preparedStatement.setInt(1, id);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			category = new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("ID"), resultSet.getString("Name"), new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("category_id"), resultSet.getString("category_name"), null));
		}
		return category;
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Category#findByName(java.lang.String)
	 */
	@Override
	public com.melkojji.model.ManagedBeans.Category findByName(String name) throws Exception {
		// TODO Auto-generated method stub
		com.melkojji.model.ManagedBeans.Category category = null;
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `categories`.`ID`, `categories`.`Name`, `categories`.`category_id`, `categories_2`.`Name` AS `category_name` FROM `categories` LEFT JOIN `categories` AS `categories_2` ON `categories`.`category_id` = `categories_2`.`ID` WHERE `categories`.`Name` = ?");
		preparedStatement.setString(1, name);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			category = new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("ID"), resultSet.getString("Name"), new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("category_id"), resultSet.getString("category_name"), null));
		}
		return category;
	}

}
