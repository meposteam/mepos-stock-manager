/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;

/**
 * @author melko
 *
 */
public class Product implements com.melkojji.model.DAO.Interfaces.Product {
	private DAOFactory DAOFactory;
	
	/**
	 * @param DAOFactory
	 */
	public Product(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}

	
	
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Product#findAll()
	 */
	@Override
	public ArrayList<com.melkojji.model.ManagedBeans.Product> findAll() throws Exception {
		ArrayList<com.melkojji.model.ManagedBeans.Product> products = new ArrayList<com.melkojji.model.ManagedBeans.Product>();
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `products`.`ID`, `products`.`BarCode`, `products`.`Name`, `products`.`Description`, `products`.`Price`, `products`.`category_id`, `categories`.`Name` AS `category_name` FROM `g_vente`.`products` LEFT JOIN `g_vente`.`categories` ON `products`.`category_id` = `categories`.`ID` WHERE 1");
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			products.add(new com.melkojji.model.ManagedBeans.Product(resultSet.getInt("ID"), resultSet.getString("BarCode"), resultSet.getString("Name"), resultSet.getString("Description"), resultSet.getFloat("Price"), new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("category_id"), resultSet.getString("category_name"), null)));
		}
		return products;
	}
}
