/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class Purchase implements com.melkojji.model.DAO.Interfaces.Purchase {
	private DAOFactory DAOFactory;
	
	/**
	 * @param DAOFactory
	 */
	public Purchase(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}
	

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Purchase#insert(com.melkojji.model.ManagedBeans.Purchase)
	 */
	@Override
	public void insert(com.melkojji.model.ManagedBeans.Purchase purchase) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `purchases` (`ID`, `product_id`, `Quantity`, `ReceptionDate`) VALUES (NULL, ?, ?, ?);");
		preparedStatement.setInt(1, purchase.getProduct().getId());
		preparedStatement.setInt(2, purchase.getQuantity());
		preparedStatement.setDate(3, purchase.getReceptionDate());
		preparedStatement.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Purchase#update(com.melkojji.model.ManagedBeans.Purchase)
	 */
	@Override
	public void update(com.melkojji.model.ManagedBeans.Purchase purchase) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE `purchases` SET `product_id` = ?, `Quantity` = ?, `ReceptionDate` = ? WHERE `purchases`.`ID` = ?;");
		preparedStatement.setInt(1, purchase.getProduct().getId());
		preparedStatement.setInt(2, purchase.getQuantity());
		preparedStatement.setDate(3, purchase.getReceptionDate());
		preparedStatement.setInt(4, purchase.getId());
		preparedStatement.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Purchase#delete(com.melkojji.model.ManagedBeans.Purchase)
	 */
	@Override
	public void delete(com.melkojji.model.ManagedBeans.Purchase purchase) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `purchases` WHERE `purchases`.`ID` = ?;");
		preparedStatement.setInt(1, purchase.getId());
		preparedStatement.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Purchase#findAll()
	 */
	@Override
	public ArrayList<com.melkojji.model.ManagedBeans.Purchase> findAll() throws Exception {
		ArrayList<com.melkojji.model.ManagedBeans.Purchase> purchases = new ArrayList<com.melkojji.model.ManagedBeans.Purchase>();
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `purchases`.`ID`, `purchases`.`product_id`, `products`.`BarCode` AS `product_barcode`, `products`.`Name` AS `product_name`, `products`.`Description` AS `product_description`, `products`.`Price` AS `product_price`, `products`.`category_id` AS `product_category_id`, `categories`.`Name` AS `product_category_name`, `purchases`.`Quantity`, `purchases`.`ReceptionDate` FROM `purchases` LEFT JOIN `g_vente`.`products` ON `purchases`.`product_id` = `products`.`ID` LEFT JOIN `g_vente`.`categories` ON `products`.`category_id` = `categories`.`ID` WHERE 1");
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			purchases.add(new com.melkojji.model.ManagedBeans.Purchase(resultSet.getInt("ID"), new com.melkojji.model.ManagedBeans.Product(resultSet.getInt("product_id"), resultSet.getString("product_barcode"), resultSet.getString("product_name"), resultSet.getString("product_description"), resultSet.getFloat("product_price"), new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("product_category_id"), resultSet.getString("product_category_name"), null)), resultSet.getInt("Quantity"), resultSet.getDate("ReceptionDate")));
		}
		return purchases;
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Purchase#findQuantityByProductAndReceptionDateLimit(com.melkojji.model.ManagedBeans.Product, java.sql.Date)
	 */
	@Override
	public Integer findQuantityByProductAndReceptionDateLimit(com.melkojji.model.ManagedBeans.Product product, Date receptionDateLimit) throws Exception {
		Integer quantity = 0;
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT SUM(`purchases`.`Quantity`) AS `Quantity` FROM `purchases` WHERE `purchases`.`product_id` = ? AND `purchases`.`ReceptionDate` >= ? AND `purchases`.`ReceptionDate` <= ?");
		preparedStatement.setInt(1, product.getId());
		preparedStatement.setDate(2, product.getLatestInventory().getDate());
		preparedStatement.setDate(3, receptionDateLimit);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			quantity = resultSet.getInt("Quantity");
		}
		return quantity;
	}
}
