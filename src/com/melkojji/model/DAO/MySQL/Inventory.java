/**
 * 
 */
package com.melkojji.model.DAO.MySQL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.melkojji.model.DAO.DAOFactory.DAOFactory;
import com.melkojji.model.ManagedBeans.Product;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class Inventory implements com.melkojji.model.DAO.Interfaces.Inventory {
	private DAOFactory DAOFactory;
	
	/**
	 * @param DAOFactory
	 */
	public Inventory(com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory) {
		this.DAOFactory = DAOFactory;
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Inventory#insert(com.melkojji.model.ManagedBeans.Inventory)
	 */
	@Override
	public void insert(com.melkojji.model.ManagedBeans.Inventory inventory) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO `inventories` (`ID`, `product_id`, `Quantity`, `Description`, `Date`) VALUES (NULL, ?, ?, ?, ?);");
		preparedStatement.setInt(1, inventory.getProduct().getId());
		preparedStatement.setInt(2, inventory.getQuantity());
		preparedStatement.setString(3, inventory.getDescription());
		preparedStatement.setDate(4, inventory.getDate());
		preparedStatement.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Inventory#update(com.melkojji.model.ManagedBeans.Inventory)
	 */
	@Override
	public void update(com.melkojji.model.ManagedBeans.Inventory inventory) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("UPDATE `inventories` SET `product_id` = ?, `Quantity` = ?, `Description` = ?, `Date` = ? WHERE `inventories`.`ID` = ?;");
		preparedStatement.setInt(1, inventory.getProduct().getId());
		preparedStatement.setInt(2, inventory.getQuantity());
		preparedStatement.setString(3, inventory.getDescription());
		preparedStatement.setDate(4, inventory.getDate());
		preparedStatement.setInt(5, inventory.getId());
		preparedStatement.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Inventory#delete(com.melkojji.model.ManagedBeans.Inventory)
	 */
	@Override
	public void delete(com.melkojji.model.ManagedBeans.Inventory inventory) throws Exception {
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM `inventories` WHERE `inventories`.`ID` = ?;");
		preparedStatement.setInt(1, inventory.getId());
		preparedStatement.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Inventory#findAll()
	 */
	@Override
	public ArrayList<com.melkojji.model.ManagedBeans.Inventory> findAll() throws Exception {
		ArrayList<com.melkojji.model.ManagedBeans.Inventory> inventories = new ArrayList<com.melkojji.model.ManagedBeans.Inventory>();
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT `inventories`.`ID`, `inventories`.`product_id`, `products`.`BarCode` AS `product_barcode`, `products`.`Name` AS `product_name`, `products`.`Description` AS `product_description`, `products`.`Price` AS `product_price`, `products`.`category_id` AS `product_category_id`, `categories`.`Name` AS `product_category_name`, `inventories`.`Quantity`, `inventories`.`Description`, `inventories`.`Date` FROM `inventories` LEFT JOIN `g_vente`.`products` ON `inventories`.`product_id` = `products`.`ID` LEFT JOIN `g_vente`.`categories` ON `products`.`category_id` = `categories`.`ID` WHERE 1");
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			inventories.add(new com.melkojji.model.ManagedBeans.Inventory(resultSet.getInt("ID"), new com.melkojji.model.ManagedBeans.Product(resultSet.getInt("product_id"), resultSet.getString("product_barcode"), resultSet.getString("product_name"), resultSet.getString("product_description"), resultSet.getFloat("product_price"), new com.melkojji.model.ManagedBeans.Category(resultSet.getInt("product_category_id"), resultSet.getString("product_category_name"), null)), resultSet.getInt("Quantity"), resultSet.getString("Description"), resultSet.getDate("Date")));
		}
		return inventories;
	}

	/* (non-Javadoc)
	 * @see com.melkojji.model.DAO.Interfaces.Inventory#findLatestByProductAndDate(com.melkojji.model.ManagedBeans.Product, java.sql.Date)
	 */
	@Override
	public com.melkojji.model.ManagedBeans.Inventory findLatestByProductAndDate(Product product, Date date) throws Exception {
		com.melkojji.model.ManagedBeans.Inventory inventory = null;
		Connection connection = this.DAOFactory.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM `inventories` WHERE `product_id` = ? AND `inventories`.`Date` <= ? ORDER BY `inventories`.`Date` DESC, `inventories`.`ID` DESC LIMIT 1");
		preparedStatement.setInt(1, product.getId());
		preparedStatement.setDate(2, date);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			inventory = new com.melkojji.model.ManagedBeans.Inventory(resultSet.getInt("ID"), product, resultSet.getInt("Quantity"), resultSet.getString("Description"), resultSet.getDate("Date"));
		}
		return inventory;
	}
}
