/**
 * 
 */
package com.melkojji.model.DAO.DAOFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Mustapha EL KOJJI
 *
 */
public class DAOFactory {
	private String url;
	private String username;
	private String password;
	
	
	/**
	 * @param url
	 * @param username
	 * @param password
	 */
	public DAOFactory(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}
	
	/**
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static DAOFactory getInstance(String url) throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		return new DAOFactory(url, "root", "");
	}
	
	/**
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(this.url, this.username, this.password);
	}
	
	public com.melkojji.model.DAO.Interfaces.User getUserDAO() {
		return new com.melkojji.model.DAO.MySQL.User(this);
	}
}
