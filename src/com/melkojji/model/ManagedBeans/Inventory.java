/**
 * 
 */
package com.melkojji.model.ManagedBeans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.PrimeFaces;

/**
 * @author Mustapha EL KOJJI
 *
 */
@ManagedBean(name="inventoryBean")
@ViewScoped
public class Inventory {
	private Integer id;
	private Product product;
	private Integer quantity;
	private String description;
	private Date date;
	private List<Inventory> inventories;
	private List<Inventory> selectedInventories;
	private List<Inventory> filteredInventories;
	private List<Product> products;
	private Inventory selectedInventory;
	
	
	/**
	 * 
	 */
	public Inventory() {
		
	}
	/**
	 * @param id
	 * @param product
	 * @param quantity
	 * @param description
	 * @param date
	 */
	public Inventory(Integer id, Product product, Integer quantity, String description, Date date) {
		super();
		this.id = id;
		this.product = product;
		this.quantity = quantity;
		this.description = description;
		this.date = date;
	}



	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the inventories
	 */
	public List<Inventory> getInventories() {
		return inventories;
	}
	/**
	 * @param inventories the inventories to set
	 */
	public void setInventories(List<Inventory> inventories) {
		this.inventories = inventories;
	}
	/**
	 * @return the selectedInventories
	 */
	public List<Inventory> getSelectedInventories() {
		return selectedInventories;
	}
	/**
	 * @param selectedInventories the selectedInventories to set
	 */
	public void setSelectedInventories(List<Inventory> selectedInventories) {
		this.selectedInventories = selectedInventories;
	}
	/**
	 * @return the filteredInventories
	 */
	public List<Inventory> getFilteredInventories() {
		return filteredInventories;
	}
	/**
	 * @param filteredInventories the filteredInventories to set
	 */
	public void setFilteredInventories(List<Inventory> filteredInventories) {
		this.filteredInventories = filteredInventories;
	}
	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}
	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	/**
	 * @return the selectedInventory
	 */
	public Inventory getSelectedInventory() {
		return selectedInventory;
	}
	/**
	 * @param selectedInventory the selectedInventory to set
	 */
	public void setSelectedInventory(Inventory selectedInventory) {
		this.selectedInventory = selectedInventory;
	}
	
	
	
	
	public List<String> completeProduct(String enteredValue) {
        ArrayList<String> matches = new ArrayList<String>();
        for (Product product : this.products) {
            if (product.getName().toLowerCase().startsWith(enteredValue.toLowerCase())) {
                matches.add(product.getName());
            }
        }
        return matches;
    }
	
	
	
	@PostConstruct
	public void init() {		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = null;
		try {
			DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		ArrayList<Inventory> inventories = new ArrayList<Inventory>();
		try {
			inventories = inventoryDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.inventories = new ArrayList<Inventory>();
		Iterator<Inventory> inventoriesIterator = inventories.iterator();
		while(inventoriesIterator.hasNext()) {
			Inventory inventory = inventoriesIterator.next();
			this.inventories.add(inventory);
			if(inventory.getId().equals(this.id)) {
				
			}
		}
		this.product = new Product();
		com.melkojji.model.DAO.MySQL.Product productDAO = new com.melkojji.model.DAO.MySQL.Product(DAOFactory);
		ArrayList<Product> products = new ArrayList<Product>();
		try {
			products = productDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.products = products;
	}
	
	
	
	public String create(ActionEvent event) throws Exception {
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		Product inventoryProduct = null;
		for(Product product : this.products) {
			if(this.product.getName().equals(product.getName())) {
				inventoryProduct = product;
			}
		}
		if(inventoryProduct == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new inventory has failed !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		inventoryDAO.insert(new Inventory(this.id, inventoryProduct, this.quantity, this.description, java.sql.Date.valueOf(LocalDateTime.now().toLocalDate())));
		this.inventories = inventoryDAO.findAll();
		this.product.setName(null);
		this.quantity = null;
		this.description = null;
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of adding a new inventory has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	
	public String update(ActionEvent event) throws Exception {		
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		Product inventoryProduct = null;
		for(Product product : this.products) {
			if(this.selectedInventory.product.getName().equals(product.getName())) {
				inventoryProduct = product;
			}
		}
		if(inventoryProduct == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of updating an inventory has failed !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		inventoryDAO.update(this.selectedInventory);
		
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of updating an inventory has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	
	public String delete() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		inventoryDAO.delete(this.selectedInventory);
		this.inventories = inventoryDAO.findAll();
		this.selectedInventory = null;
		return "success";
	}
	
	public String deleteSelectedRows() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		for(Inventory inventory : this.selectedInventories) {
			inventoryDAO.delete(inventory);
		}
		this.inventories = inventoryDAO.findAll();
		this.selectedInventories = null;
		return "success";
	}
}
