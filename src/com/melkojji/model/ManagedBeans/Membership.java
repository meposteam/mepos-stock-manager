/**
 * 
 */
package com.melkojji.model.ManagedBeans;

import javax.faces.bean.ManagedBean;

/**
 * @author Mustapha EL KOJJI
 *
 */
@ManagedBean
public class Membership {
	private Integer id;
	private String name;
	
	
	/**
	 * 
	 */
	public Membership() {
		
	}
	/**
	 * @param id
	 * @param name
	 */
	public Membership(Integer id, String name) {
		this.id = id;
		this.name = name;
	}



	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}
}
