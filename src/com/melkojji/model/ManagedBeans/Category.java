/**
 * 
 */
package com.melkojji.model.ManagedBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.PrimeFaces;

/**
 * @author Mustapha EL KOJJI
 *
 */
@ManagedBean(name="categoryBean")
@ViewScoped
public class Category {
	private Integer id;
	private String name;
	private Category parentCategory;
	private List<Category> categories;
	private List<Category> selectedCategories;
	private List<Category> filteredCategories;
	private Category selectedCategory;
	
	
	/**
	 * 
	 */
	public Category() {
		
	}
	/**
	 * @param id
	 * @param name
	 * @param parentCategory
	 */
	public Category(Integer id, String name, Category parentCategory) {
		this.id = id;
		this.name = name;
		this.parentCategory = parentCategory;
	}



	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the parentCategory
	 */
	public Category getParentCategory() {
		return parentCategory;
	}
	/**
	 * @param parentCategory the parentCategory to set
	 */
	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}
	/**
	 * @return the categories
	 */
	public List<Category> getCategories() {
		return categories;
	}
	/**
	 * @param categories the categories to set
	 */
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	/**
	 * @return the selectedCategories
	 */
	public List<Category> getSelectedCategories() {
		return selectedCategories;
	}
	/**
	 * @param selectedCategories the selectedCategories to set
	 */
	public void setSelectedCategories(List<Category> selectedCategories) {
		this.selectedCategories = selectedCategories;
	}
	/**
	 * @return the filteredCategories
	 */
	public List<Category> getFilteredCategories() {
		return filteredCategories;
	}
	/**
	 * @param filteredCategories the filteredCategories to set
	 */
	public void setFilteredCategories(List<Category> filteredCategories) {
		this.filteredCategories = filteredCategories;
	}
	/**
	 * @return the selectedCategory
	 */
	public Category getSelectedCategory() {
		return selectedCategory;
	}
	/**
	 * @param selectedCategory the selectedCategory to set
	 */
	public void setSelectedCategory(Category selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
	
	
	
	public List<String> completeCategory(String enteredValue) {
        ArrayList<String> matches = new ArrayList<String>();
        for (Category category : this.categories) {
            if (category.getName().toLowerCase().startsWith(enteredValue.toLowerCase())) {
                matches.add(category.getName());
            }
        }
        return matches;
    }
	
	
	@PostConstruct
	public void init() {
		this.parentCategory = new Category();
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = null;
		try {
			DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Category categoryDAO = new com.melkojji.model.DAO.MySQL.Category(DAOFactory);
		try {
			this.categories = categoryDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String create(ActionEvent event) throws Exception {
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.Category categoryDAO = new com.melkojji.model.DAO.MySQL.Category(DAOFactory);
		Category categoryParentCategory = null;
		Category categoryNameHolder = null;
		for(Category category : this.categories) {
			if(this.parentCategory.getName().equals(category.getName())) {
				categoryParentCategory = category;
			}
			if(this.name.equals(category.getName())) {
				categoryNameHolder = category;
			}
		}
		if(categoryParentCategory == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new category has failed ! Please select an existant parent category !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		if(categoryNameHolder != null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new category has failed ! There is a category with the same name !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		if(this.name.equals("")) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new category has failed ! The category name can not be null !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		categoryDAO.insert(new Category(this.id, this.name, categoryParentCategory));
		this.categories = categoryDAO.findAll();
		this.parentCategory.setName(null);
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of adding a new category has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	public String update(ActionEvent event) throws Exception {
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.Category categoryDAO = new com.melkojji.model.DAO.MySQL.Category(DAOFactory);
		Category categoryNameHolder = categoryDAO.findByName(this.selectedCategory.name);
		Category parentCategoryNameHolder = categoryDAO.findByName(this.selectedCategory.parentCategory.name);
		if(parentCategoryNameHolder == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of updating a category has failed ! There is no category with the same name !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			this.categories = categoryDAO.findAll();
			return "error";
		}
		if(categoryNameHolder != null) {
			if(!categoryNameHolder.getId().equals(this.selectedCategory.id)) {
				message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of updating a category has failed ! There is a category with the same name and different id !");
				FacesContext.getCurrentInstance().addMessage(null, message);
				PrimeFaces.current().ajax().addCallbackParam("success", success);
				this.categories = categoryDAO.findAll();
				return "error";
			}
		}
		categoryDAO.update(new Category(this.selectedCategory.id, this.selectedCategory.name, this.selectedCategory.parentCategory));
		
		this.categories = categoryDAO.findAll();
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of updating a category has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	public String delete() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.Category categoryDAO = new com.melkojji.model.DAO.MySQL.Category(DAOFactory);
		categoryDAO.delete(this.selectedCategory);
		this.categories = categoryDAO.findAll();
		this.selectedCategories = null;
		return "success";
	}
	public String deleteSelectedRows() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.Category categoryDAO = new com.melkojji.model.DAO.MySQL.Category(DAOFactory);
		for(Category category : this.selectedCategories) {
			categoryDAO.delete(category);
		}
		this.categories = categoryDAO.findAll();
		this.selectedCategories = null;
		return "success";
	}
}
