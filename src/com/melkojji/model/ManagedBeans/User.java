/**
 * 
 */
package com.melkojji.model.ManagedBeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;


/**
 * @author Mustapha EL KOJJI
 *
 */
@ManagedBean(name="userBean")
@SessionScoped
public class User {
	private Integer id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private Membership membership;
	private List<Membership> memberships;
	private User selectedUser;
	private List<User> users;
	private List<User> selectedUsers;
	private List<User> filterededUsers;
	private User user;
	
	
	/**
	 * 
	 */
	public User() {
		
	}
	/**
	 * @param id
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param membership
	 */
	public User(Integer id, String username, String password, String firstName, String lastName, Membership membership) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.membership = membership;
	}


	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the membership
	 */
	public Membership getMembership() {
		return membership;
	}
	/**
	 * @param membership the membership to set
	 */
	public void setMembership(Membership membership) {
		this.membership = membership;
	}
	/**
	 * @return the memberships
	 */
	public List<Membership> getMemberships() {
		return memberships;
	}
	/**
	 * @param memberships the memberships to set
	 */
	public void setMemberships(List<Membership> memberships) {
		this.memberships = memberships;
	}
	/**
	 * @return the users
	 */
	public List<User> getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(List<User> users) {
		this.users = users;
	}
	/**
	 * @return the selectedUser
	 */
	public User getSelectedUser() {
		return selectedUser;
	}
	/**
	 * @param selectedUser the selectedUser to set
	 */
	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}
	/**
	 * @return the selectedUsers
	 */
	public List<User> getSelectedUsers() {
		return selectedUsers;
	}
	/**
	 * @param selectedUsers the selectedUsers to set
	 */
	public void setSelectedUsers(List<User> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}
	/**
	 * @return the filterededUsers
	 */
	public List<User> getFilterededUsers() {
		return filterededUsers;
	}
	/**
	 * @param filterededUsers the filterededUsers to set
	 */
	public void setFilterededUsers(List<User> filterededUsers) {
		this.filterededUsers = filterededUsers;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	@PostConstruct
	public void init() {
		// This variable will be used to manipulate user creation because the managed bean is sessionScoped and contains the logged in user
		this.user = new User();
		this.user.setMembership(new Membership());
		this.selectedUser = new User();
		this.selectedUser.setMembership(new Membership());
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = null;
		try {
			DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.User userDAO = new com.melkojji.model.DAO.MySQL.User(DAOFactory);
		try {
			this.users = userDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Membership membershipDAO = new com.melkojji.model.DAO.MySQL.Membership(DAOFactory);
		try {
			this.memberships = membershipDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public String signIn() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        //String requestURI = request.getRequestURI();
        //System.out.println(requestURI+"----");
        //String referrer = context.getExternalContext().getRequestHeaderMap().get("referer");
        //System.out.println(referrer+"----");
        try {
            request.login(this.username, this.password);
        } catch (ServletException e) {
            e.printStackTrace();
            context.addMessage(null, new FacesMessage("Login failed."));
            return "error";
        }
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposselling");
		com.melkojji.model.DAO.MySQL.User userDAO = new com.melkojji.model.DAO.MySQL.User(DAOFactory);
		User user = userDAO.findByUsername(this.username);
		if(user != null) {
			if(user.getPassword().equals(this.password) && user.getMembership().getName().equals("Administrator")) {
				this.firstName = user.getFirstName();
				this.lastName = user.getLastName();
				this.membership = user.getMembership();
				//context.getExternalContext().redirect(referrer);
				//return "success";
				return "/secured/Product/List.xhtml?faces-redirect=true";
			}
			else {
				return "error";
			}
		}
		return "error";
	}
	
	public String signOut() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.invalidate();
		//return "SignIn";
		//FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/SignIn.xhtml?faces-redirect=true";
        //return "success";
	}
	
	public String create(ActionEvent event) throws Exception {
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.User userDAO = new com.melkojji.model.DAO.MySQL.User(DAOFactory);
		User userUsernameHolder = null;
		for(User user : this.users) {
			if(this.user.getUsername().equals(user.getUsername())) {
				userUsernameHolder = user;
			}
		}
		if(userUsernameHolder != null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new user profile has failed ! There is a user with the same username !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		if(this.user.getUsername().equals("") || this.user.getPassword().equals("")) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new user profile has failed ! The username and/or password can not be null !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		userDAO.insert(this.user);
		this.users = userDAO.findAll();
		this.user = new User();
		this.user.setMembership(new Membership());
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of adding a new category has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	
	public String update(ActionEvent event) throws Exception {
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.User userDAO = new com.melkojji.model.DAO.MySQL.User(DAOFactory);
		User userUsernameHolder = userDAO.findByUsername(this.selectedUser.username);
		if(userUsernameHolder != null) {
			if(!userUsernameHolder.getId().equals(this.selectedUser.id)) {
				message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of updating a user profile has failed ! There is a user with the same username and different id !");
				FacesContext.getCurrentInstance().addMessage(null, message);
				PrimeFaces.current().ajax().addCallbackParam("success", success);
				this.users = userDAO.findAll();
				return "error";
			}
		}
		if(this.selectedUser.membership.getId() == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of updating a user profile has failed ! The membership can not be null !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			this.users = userDAO.findAll();
			return "error";
		}
		
		for(Membership membership : this.memberships) {
			if(this.selectedUser.membership.getId().equals(membership.getId())) {
				this.selectedUser.setMembership(membership);
			}
		}
		userDAO.update(this.selectedUser);
		this.users = userDAO.findAll();
		//this.selectedUser = new User();
		//this.selectedUser.setMembership(new Membership());
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of updating a user profile has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		return "success";
	}
	
	public String delete() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.User userDAO = new com.melkojji.model.DAO.MySQL.User(DAOFactory);
		userDAO.delete(this.selectedUser);
		this.users = userDAO.findAll();
		this.selectedUser = null;
		return "success";
	}
	
	public String deleteSelectedRows() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/g_vente");
		com.melkojji.model.DAO.MySQL.User userDAO = new com.melkojji.model.DAO.MySQL.User(DAOFactory);
		for(User user : this.selectedUsers) {
			userDAO.delete(user);
		}
		this.users = userDAO.findAll();
		this.selectedUsers = null;
		return "success";
	}
}
