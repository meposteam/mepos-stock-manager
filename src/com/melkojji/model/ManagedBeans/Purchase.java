/**
 * 
 */
package com.melkojji.model.ManagedBeans;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.PrimeFaces;

/**
 * @author Mustapha EL KOJJI
 *
 */
@ManagedBean(name="purchaseBean")
@ViewScoped
public class Purchase {
	private Integer id;
	private Product product;
	private Integer quantity;
	private Date receptionDate;
	private List<Purchase> purchases;
	private List<Purchase> selectedPurchases;
	private List<Purchase> filteredPurchases;
	private List<Product> products;
	private Purchase selectedPurchase;
	
	
	
	/**
	 * 
	 */
	public Purchase() {
		
	}
	/**
	 * @param id
	 * @param product
	 * @param quantity
	 * @param receptionDate
	 */
	public Purchase(Integer id, Product product, Integer quantity, Date receptionDate) {
		super();
		this.id = id;
		this.product = product;
		this.quantity = quantity;
		this.receptionDate = receptionDate;
	}
	
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the receptionDate
	 */
	public Date getReceptionDate() {
		return receptionDate;
	}
	/**
	 * @param receptionDate the receptionDate to set
	 */
	public void setReceptionDate(Date receptionDate) {
		this.receptionDate = receptionDate;
	}
	/**
	 * @return the purchases
	 */
	public List<Purchase> getPurchases() {
		return purchases;
	}
	/**
	 * @param purchases the purchases to set
	 */
	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}
	/**
	 * @return the selectedPurchases
	 */
	public List<Purchase> getSelectedPurchases() {
		return selectedPurchases;
	}
	/**
	 * @param selectedPurchases the selectedPurchases to set
	 */
	public void setSelectedPurchases(List<Purchase> selectedPurchases) {
		this.selectedPurchases = selectedPurchases;
	}
	/**
	 * @return the filteredPurchases
	 */
	public List<Purchase> getFilteredPurchases() {
		return filteredPurchases;
	}
	/**
	 * @param filteredPurchases the filteredPurchases to set
	 */
	public void setFilteredPurchases(List<Purchase> filteredPurchases) {
		this.filteredPurchases = filteredPurchases;
	}
	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}
	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	/**
	 * @return the selectedPurchase
	 */
	public Purchase getSelectedPurchase() {
		return selectedPurchase;
	}
	/**
	 * @param selectedPurchase the selectedPurchase to set
	 */
	public void setSelectedPurchase(Purchase selectedPurchase) {
		this.selectedPurchase = selectedPurchase;
	}
	
	
	
	public List<String> completeProduct(String enteredValue) {
        ArrayList<String> matches = new ArrayList<String>();
        for (Product product : this.products) {
            if (product.getName().toLowerCase().startsWith(enteredValue.toLowerCase())) {
                matches.add(product.getName());
            }
        }
        return matches;
    }



	@PostConstruct
	public void init() {
		this.product = new Product();
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = null;
		try {
			DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		try {
			this.purchases = purchaseDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Product productDAO = new com.melkojji.model.DAO.MySQL.Product(DAOFactory);
		try {
			this.products = productDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String create(ActionEvent event) throws Exception {
		System.out.println("Start Create");
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		Product purchaseProduct = null;
		for(Product product : this.products) {
			if(this.product.getName().equals(product.getName())) {
				purchaseProduct = product;
			}
		}
		if(purchaseProduct == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of adding a new purchase has failed !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		purchaseDAO.insert(new Purchase(this.id, purchaseProduct, this.quantity, java.sql.Date.valueOf(LocalDateTime.now().toLocalDate())));
		this.purchases = purchaseDAO.findAll();
		this.product.setName(null);
		this.quantity = null;
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of adding a new purchase has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	
	public String update(ActionEvent event) throws Exception {		
		FacesMessage message = null;
		boolean success = false;
		
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		Product purchaseProduct = null;
		for(Product product : this.products) {
			if(this.selectedPurchase.product.getName().equals(product.getName())) {
				purchaseProduct = product;
			}
		}
		if(purchaseProduct == null) {
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Message", "The process of updating a purchase has failed !");
			FacesContext.getCurrentInstance().addMessage(null, message);
			PrimeFaces.current().ajax().addCallbackParam("success", success);
			return "error";
		}
		purchaseDAO.update(this.selectedPurchase);
		
		success = true;
		message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", "The process of updating a purchase has succeeded !");
		FacesContext.getCurrentInstance().addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("success", success);
		
		return "success";
	}
	
	public String delete() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		purchaseDAO.delete(this.selectedPurchase);
		this.purchases = purchaseDAO.findAll();
		this.selectedPurchase = null;
		return "success";
	}
	
	public String deleteSelectedRows() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		for(Purchase purchase : this.selectedPurchases) {
			purchaseDAO.delete(purchase);
		}
		this.purchases = purchaseDAO.findAll();
		this.selectedPurchases = null;
		return "success";
	}
}
