/**
 * 
 */
package com.melkojji.model.ManagedBeans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author Mustapha EL KOJJI
 *
 */
@ManagedBean(name="productBean")
@ViewScoped
public class Product {
	private Integer id;
	private String barcode;
	private String name;
	private String description;
	private Integer quantity;
	private Float price;
	private Category category;
	private List<Product> products;
	private List<Product> filteredProducts;
	private Inventory latestInventory;
	private Date historyDate;
	private List<Product> historyDateStockProducts;
	
	
	/**
	 * 
	 */
	public Product() {
		
	}
	/**
	 * @param id
	 * @param barcode
	 * @param name
	 * @param description
	 * @param price
	 * @param category
	 */
	public Product(Integer id, String barcode, String name, String description, Float price, Category category) {
		this.id = id;
		this.barcode = barcode;
		this.name = name;
		this.description = description;
		this.price = price;
		this.category = category;
	}




	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}
	/**
	 * @param barcode the barcode to set
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public Float getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Float price) {
		this.price = price;
	}
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}
	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	/**
	 * @return the filteredProducts
	 */
	public List<Product> getFilteredProducts() {
		return filteredProducts;
	}
	/**
	 * @param filteredProducts the filteredProducts to set
	 */
	public void setFilteredProducts(List<Product> filteredProducts) {
		this.filteredProducts = filteredProducts;
	}
	/**
	 * @return the latestInventory
	 */
	public Inventory getLatestInventory() {
		return latestInventory;
	}
	/**
	 * @param latestInventory the latestInventory to set
	 */
	public void setLatestInventory(Inventory latestInventory) {
		this.latestInventory = latestInventory;
	}
	/**
	 * @return the historyDate
	 */
	public Date getHistoryDate() {
		return historyDate;
	}
	/**
	 * @param historyDate the historyDate to set
	 */
	public void setHistoryDate(Date historyDate) {
		this.historyDate = historyDate;
	}
	/**
	 * @return the historyDateStockProducts
	 */
	public List<Product> getHistoryDateStockProducts() {
		return historyDateStockProducts;
	}
	/**
	 * @param historyDateStockProducts the historyDateStockProducts to set
	 */
	public void setHistoryDateStockProducts(List<Product> historyDateStockProducts) {
		this.historyDateStockProducts = historyDateStockProducts;
	}
	
	
	
	@PostConstruct
	public void init() {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = null;
		try {
			DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Product productDAO = new com.melkojji.model.DAO.MySQL.Product(DAOFactory);
		try {
			this.products = productDAO.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		com.melkojji.model.DAO.MySQL.Order orderDAO = new com.melkojji.model.DAO.MySQL.Order(DAOFactory);
		this.historyDateStockProducts = new ArrayList<Product>();
		for(Product product : this.products) {
			Inventory inventory = null;
			try {
				inventory = inventoryDAO.findLatestByProductAndDate(product, java.sql.Date.valueOf(LocalDateTime.now().toLocalDate()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(inventory != null) {
				product.setLatestInventory(inventory);
				this.historyDateStockProducts.add(product);
			}
		}
		for(Product product : this.historyDateStockProducts) {
			Integer purchaseQuantity = 0;
			try {
				purchaseQuantity = purchaseDAO.findQuantityByProductAndReceptionDateLimit(product, java.sql.Date.valueOf(LocalDateTime.now().toLocalDate()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			Integer orderQuantity = 0;
			try {
				orderQuantity = orderDAO.findQuantityByProductAndDateLimit(product, java.sql.Date.valueOf(LocalDateTime.now().toLocalDate()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			product.setQuantity(product.getLatestInventory().getQuantity() + purchaseQuantity - orderQuantity);
		}
	}
	public void filterByHistoryDate() throws Exception {
		com.melkojji.model.DAO.DAOFactory.DAOFactory DAOFactory = null;
		try {
			DAOFactory = com.melkojji.model.DAO.DAOFactory.DAOFactory.getInstance("jdbc:mysql://localhost:3306/meposstock");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		com.melkojji.model.DAO.MySQL.Inventory inventoryDAO = new com.melkojji.model.DAO.MySQL.Inventory(DAOFactory);
		com.melkojji.model.DAO.MySQL.Purchase purchaseDAO = new com.melkojji.model.DAO.MySQL.Purchase(DAOFactory);
		com.melkojji.model.DAO.MySQL.Order orderDAO = new com.melkojji.model.DAO.MySQL.Order(DAOFactory);
		this.historyDateStockProducts = new ArrayList<Product>();
		for(Product product : this.products) {
			Inventory inventory = null;
			try {
				inventory = inventoryDAO.findLatestByProductAndDate(product, new java.sql.Date(this.historyDate.getTime()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(inventory != null) {
				product.setLatestInventory(inventory);
				this.historyDateStockProducts.add(product);
			}
		}
		for(Product product : this.historyDateStockProducts) {
			Integer purchaseQuantity = 0;
			try {
				purchaseQuantity = purchaseDAO.findQuantityByProductAndReceptionDateLimit(product, new java.sql.Date(this.historyDate.getTime()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			Integer orderQuantity = 0;
			try {
				orderQuantity = orderDAO.findQuantityByProductAndDateLimit(product, new java.sql.Date(this.historyDate.getTime()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			product.setQuantity(product.getLatestInventory().getQuantity() + purchaseQuantity - orderQuantity);
		}
	}
}
